def factors(number):
    # ==============
    # Your code here
    answer = []
    for count in range(2, number):
        x = number % count
        if x == 0:
            answer.append(count)
    if answer == []:
        answer = str(number) + " is a prime number"
    return answer
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
