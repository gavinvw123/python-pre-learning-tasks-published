def calculator(a, b, operator):
    # ==============
    # Your code here
    import math
    if operator == "+":
        answer = math.floor(a + b)
        bin_answer = bin(answer)[2:]
    elif operator == "-":
        answer = math.floor(a - b)
        bin_answer = bin(answer)[2:]
    elif operator == "*":
        answer = math.floor(a * b)
        bin_answer = bin(answer)[2:]
    elif operator == "/":
        answer = math.floor(a / b)
        bin_answer = bin(answer)[2:]

    return bin_answer
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
